﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileUtilsScript : MonoBehaviour
{
    public bool DOEnableFPS = false;

    private int FramesPerSec;
    private float frequency = 1.0f;
    private string fps;


    void Start()
    {
        Application.targetFrameRate = 60;
        if (DOEnableFPS)
            StartCoroutine(FPS());
    }

    private IEnumerator FPS()
    {
        for (; ; )
        {
            // Capture frame-per-second
            int lastFrameCount = Time.frameCount;
            float lastTime = Time.realtimeSinceStartup;
            yield return new WaitForSeconds(frequency);
            float timeSpan = Time.realtimeSinceStartup - lastTime;
            int frameCount = Time.frameCount - lastFrameCount;

            // Display it

            fps = string.Format("FPS: {0}", Mathf.RoundToInt(frameCount / timeSpan));
        }
    }


    void OnGUI()
    {
        if (DOEnableFPS)
            GUI.Label(new Rect(Screen.width - 200, 20, 200, 60), fps);
    }
}
