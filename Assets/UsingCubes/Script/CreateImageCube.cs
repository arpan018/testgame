﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CreateImageCube : MonoBehaviour
{
    public static CreateImageCube instance;

    public GameObject pixelCubePrefab;
    public GameObject fillCubePrefab;
    public Texture2D imageToRead;
    public Transform imageParent;
    public Transform imageParent2;
    public Slider scoreSlider;
    public Text lvlComplete;
    public Transform[] spawnPoints;


    private GameObject[] transparentWall;
    int totalCubes;
    int filledCubes;
    public static bool isFillDone;


    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        createImagePixel();
        StartCoroutine(GenCubes());
    }

    public IEnumerator GenCubes()
    {
        // total 308 cubes
        //for (int i = 0; i < 320; i++)
        //{
        //    var Cubes = Instantiate(fillCubePrefab, spawnPoints[Random.Range(0, spawnPoints.Length)].position, Quaternion.identity);
        //    yield return new WaitForSeconds(0.1f);
        //}
            yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < imageToRead.width; i++)
        {
            for (int j = 0; j < imageToRead.height; j++)
            {
                Color pixel = imageToRead.GetPixel(i, j);
                if (pixel.a != 0)
                {
                    var cube1 = Instantiate(fillCubePrefab);
                    cube1.transform.parent = imageParent2;
                    cube1.transform.localPosition = new Vector3(i, j, 0);
                }
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    private void createImagePixel()
    {
        Color[] pixels = imageToRead.GetPixels();
        transparentWall = new GameObject[pixels.Length];

        for (int index = 0; index < pixels.Length; index++)
        {
            if (pixels[index].a != 0)
            {
                transparentWall[index] = Instantiate(pixelCubePrefab);
                transparentWall[index].transform.parent = imageParent;
                transparentWall[index].transform.localPosition = new Vector3(index % imageToRead.width,
                    index / imageToRead.width, 0);
                transparentWall[index].GetComponent<MeshRenderer>().material.color = pixels[index];
                transparentWall[index].GetComponent<MeshRenderer>().enabled = false;
                totalCubes++;
            }
        }
    }

    public void incrementFill()
    {
        filledCubes++;
        scoreSlider.value = (filledCubes / 308.0f) * 100.0f;

        if (filledCubes == totalCubes)
        {
            Debug.Log("fill complete");
            lvlComplete.gameObject.SetActive(true);
            isFillDone = true;
        }
    }
}
