﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rigidBody;
    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector2 startPos;
    private bool isSwiping;
    private Vector3 direction;
    private Vector2 currentSwipeDelta;
    private Vector2 previousSwipeDelta;
    private Vector3 moveDirection;
    public float movementSpeed;
    private Vector3 newDir;
    private float swipeStartTime;
    private float swipeEndTime;
    private float swipeInterval;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
    }

    private void PlayerInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            //GameManager.Instance.StartGame();
        }

        if (Input.GetMouseButton(0))
        {
            isSwiping = true;
            direction = Input.mousePosition - (Vector3)startPos;
            direction = Vector3.Normalize(direction);
        }

        if (Input.GetMouseButtonUp(0))
        {
            isSwiping = false;
            direction = Vector2.zero;
            rigidBody.velocity = Vector3.zero;
        }

        currentSwipeDelta = Vector2.zero;

        if (isSwiping)
        {
            currentSwipeDelta = Input.mousePosition - (Vector3)startPos;
        }

        //transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, -5.9f, 5.9f), transform.position.y, 
        //    Mathf.Clamp(transform.localPosition.z, -1f, 21f));
    }

    private void FixedUpdate()
    {
        if (currentSwipeDelta.magnitude > 30)
        {
            moveDirection.x = direction.x;
            moveDirection.z = direction.y;
            moveDirection.y = 0f;

            // Rotation
            Quaternion newRotation = Quaternion.LookRotation(moveDirection);
            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0f, 360f, 0f) * Time.deltaTime);
            rigidBody.MoveRotation(newRotation * deltaRotation);

            // Move
            rigidBody.velocity = moveDirection * movementSpeed;
        }
    }
}
