﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSurface : MonoBehaviour
{
    public GameObject[] itemtopicfrom;
    public GameObject _cube;
    public Transform surfaceParent;
    public Vector3 _gridOrigin;
    private int gridx = 60;
    private int gridz = 30;
    private float gridspacing=1;

    void Start()
    {
        spawnGrid();
    }

    public void spawnGrid()
    {
        for (int x = 0; x < gridx; x++)
        {
            for (int z = 0; z < gridz; z++)
            {
                Vector3 spawPosition = new Vector3(x * gridspacing, 1, z * gridspacing) + _gridOrigin;
                Instantiate(_cube, spawPosition, Quaternion.identity,surfaceParent);
            }
        }
    }

}
