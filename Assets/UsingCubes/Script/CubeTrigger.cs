﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "fillcube")
        {
            GetComponent<MeshRenderer>().enabled = true;
            other.gameObject.SetActive(false);
            this.GetComponent<BoxCollider>().enabled = false;
            CreateImageCube.instance.incrementFill();
        }

        if (other.gameObject.tag == "surface")
        {
            if (Vector3.Distance(other.transform.position, transform.position) < 0.95f)
            {
                Destroy(other.gameObject);
            }
        }
    }
}
