﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderTimer : MonoBehaviour
{
    [Tooltip("How long does it take for this slider to fill, in seconds")]
    public float FillTime = 1.0f;
    public GameObject loaderPane;
    
    private Slider _slider;
    private bool isReached;

    void Start()
    {
        _slider = GetComponent<Slider>();
        Reset();
    }

    public void Reset()
    {
        isReached = false;
        _slider.minValue = Time.time;
        _slider.maxValue = Time.time + FillTime;
    }

    void Update()
    {
        if (isReached)
            return;
        _slider.value = Time.time;
        if(_slider.value >= _slider.maxValue)
        {
            isReached = true;
            loaderPane.SetActive(false);
        }
    }
}