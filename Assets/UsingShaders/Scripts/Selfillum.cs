﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selfillum : MonoBehaviour
{
    public Color selfColor;
    public Material mattoset;

    private bool isFilled;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("fillcube") && !isFilled)
        {
            isFilled = true;
            gameObject.GetComponent<MeshRenderer>().material = mattoset;
            gameObject.GetComponent<MeshRenderer>().material.color = selfColor;
            CretePixelCube.instance.incrementFill();
            Destroy(other.gameObject);
        }
    }
}
