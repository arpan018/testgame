﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mousecontroller : MonoBehaviour
{
    public float speed = 10f;
    public Vector3 targetPos;
    public bool isMoving;

    private Vector3 mousePosition;
    private Rigidbody rb;
    private Vector2 direction;
    private float moveSpeed = 100f;
    Vector3 startpos;
    Vector3 endpos;
    Vector3 diff;

    void Start()
    {

        targetPos = transform.position;
        isMoving = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startpos = Input.mousePosition;
        }

        // if (Input.GetMouseButtonDown(0))
        //if (Input.GetMouseButtonUp(0))
        if (Input.GetMouseButton(0))
        {
            endpos = Input.mousePosition;
            isMoving = false;
        }
        diff = (endpos - startpos);

        var mag = diff.magnitude;
        if (mag > 10)
        {
            isMoving = true;
            SetTarggetPosition();
        }
        else
            isMoving = false;
    }

    //void LateUpdate()
    void FixedUpdate()
    {
        if (isMoving)
        {
            MoveObject();
        }
    }

    void SetTarggetPosition()
    {
        if (isMoving)
        {
            Plane plane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float point = 0f;

            if (plane.Raycast(ray, out point))
                targetPos = ray.GetPoint(point);

            isMoving = true;
        }
    }
    void MoveObject()
    {
        transform.LookAt(targetPos);
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        //GetComponent<Rigidbody>().MovePosition(targetPos * Time.deltaTime * 10);
        if (transform.position == targetPos)
            isMoving = false;
        Debug.DrawLine(transform.position, targetPos, Color.red);
    }
}
