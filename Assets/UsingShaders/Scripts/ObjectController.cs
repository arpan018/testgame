﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectController : MonoBehaviour
{
    private float baseAngle = 0.0f;

    //private void OnMouseDown()
    //{
    //    //Debug.Log("mouse down");
    //    Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
    //    pos = Input.mousePosition - pos;
    //    baseAngle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
    //    baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
    //}

    //private void OnMouseDrag()
    //{
    //    //Debug.Log("mouse drag");
    //    Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
    //    pos = Input.mousePosition - pos;
    //    float ang = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - baseAngle;
    //    transform.rotation = Quaternion.AngleAxis(ang, -Vector3.up);
    //}

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos = Input.mousePosition - pos;
            baseAngle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
            baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.AngleAxis(baseAngle, -Vector3.up);
        }

        if (Input.GetMouseButton(0)) {
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos = Input.mousePosition - pos;
            float ang = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - baseAngle;
            transform.rotation = Quaternion.AngleAxis(ang, -Vector3.up);
            transform.Translate(Vector3.forward * pos.magnitude * 0.0012f);
        }

        if (Input.GetMouseButtonUp(0)) {

        }

        // arrow keys working version
        //float moveHorizontal = Input.GetAxis("Horizontal");
        //float moveVertical = Input.GetAxis("Vertical");
        //sliderObj.Translate(0.0f, 0.0f, moveVertical * 0.8f);
        //sliderObj.Rotate(0.0f, moveHorizontal * 5, 0.0f);
    }
}
