﻿using UnityEngine;
using UnityEngine.UI;

public class CretePixelCube : MonoBehaviour
{
    public static CretePixelCube instance;

    public Texture2D imageToRead;
    public Transform parent1;
    public Material depthmat;
    public Material transparentmat;
    public GameObject cubePrefab;
    public Slider scoreSlider;
    public Text lvlComplete;

    private GameObject[] transparentWall;
    int totalCubes;
    int filledCubes;
    public static bool isFillDone;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        ColorCubeSpawner();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void ColorCubeSpawner()
    {
        Color[] pixels = imageToRead.GetPixels();
        transparentWall = new GameObject[pixels.Length];

        for (int index = 0; index < pixels.Length; index++)
        {
            if (pixels[index].a != 0)
            {
                //transparentWall[index] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                transparentWall[index] = Instantiate(cubePrefab);
                transparentWall[index].transform.parent = parent1;
                transparentWall[index].transform.localPosition = new Vector3(index % imageToRead.width, index / imageToRead.width, 0);
                transparentWall[index].GetComponent<MeshRenderer>().sharedMaterial = depthmat;
                //transparentWall[index].GetComponent<MeshRenderer>().material.color = pixels[index];
                transparentWall[index].GetComponent<Selfillum>().mattoset = transparentmat;
                transparentWall[index].GetComponent<Selfillum>().selfColor = pixels[index];
                totalCubes++;
                //transparentWall[index].GetComponent<MeshRenderer>().material = depthmat;
                //transparentWall[index].GetComponent<MeshRenderer>().material.renderQueue = 1999;
            }
        }
        Debug.Log("required cubes " + totalCubes);
    }

    public void incrementFill()
    {
        filledCubes++;
        scoreSlider.value = (filledCubes  / 308.0f) * 100.0f;

        if (filledCubes == totalCubes)
        {
            Debug.Log("fill complete");
            lvlComplete.gameObject.SetActive(true);
            isFillDone = true;
        }
    }
}
