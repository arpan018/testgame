﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spwancubes : MonoBehaviour
{
    public GameObject spawnerPrefab;
    public Transform[] spwanLocations;
    public Transform parentHolder;

    private int spawncount = 0;

    void Start()
    {
        InvokeRepeating("spawncubes", 0, 0.2f);
    }

    void spawncubes()
    {
        spawncount++;
        GameObject go = Instantiate(spawnerPrefab, parentHolder);
        go.transform.position = spwanLocations[Random.Range(0, spwanLocations.Length)].position;
        if (spawncount > 320)
            CancelInvoke("spawncubes");
    }
}
